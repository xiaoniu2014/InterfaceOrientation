//
//  HWTabBarController.m
//  横竖屏处理
//
//  Created by 洪伟 on 2017/4/21.
//  Copyright © 2017年 yonyou. All rights reserved.
//

#import "HWTabBarController.h"

@interface HWTabBarController ()

@end

@implementation HWTabBarController


-(BOOL)shouldAutorotate{
    UINavigationController *nav = self.selectedViewController;
    return [nav.topViewController shouldAutorotate];
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    UINavigationController *nav = self.selectedViewController;
    return [nav.topViewController supportedInterfaceOrientations];;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
}

@end
