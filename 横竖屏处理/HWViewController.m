//
//  HWViewController.m
//  横竖屏处理
//
//  Created by 洪伟 on 2017/4/21.
//  Copyright © 2017年 yonyou. All rights reserved.
//

#import "HWViewController.h"

@interface HWViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (assign, nonatomic) CGSize screenSize;
@end

@implementation HWViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _screenSize = [UIScreen mainScreen].bounds.size;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"11.pdf" ofType:nil];
    
    NSURL *url = [NSURL fileURLWithPath:path];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    
//    [_webView loadRequest:request];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    [self.webView loadData:data MIMEType:@"application/pdf" textEncodingName:@"UTF-8" baseURL:url];
}


-(BOOL)shouldAutorotate{
    return YES;
}

//支持的方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    CGFloat screenW = _screenSize.width < _screenSize.height ? _screenSize.width : _screenSize.height;
    BOOL isLandscape = size.width != screenW;
    
    // 翻转的时间
//    CGFloat duration = [coordinator transitionDuration];
    [self.navigationController setNavigationBarHidden:isLandscape animated:YES];
}


@end
