//
//  HWNavgationController.m
//  横竖屏处理
//
//  Created by 洪伟 on 2017/4/21.
//  Copyright © 2017年 yonyou. All rights reserved.
//

#import "HWNavgationController.h"

@interface HWNavgationController ()

@end

@implementation HWNavgationController

-(BOOL)shouldAutorotate{
    return [self.topViewController shouldAutorotate];
}
//支持的方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}
@end
